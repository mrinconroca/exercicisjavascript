**Classe**

Una classe es una definició o explicació d’un element, sense ser l’element en si. Descriuen atributs i el seu comportament. La clase es l’idea d’un objecte.

**Objecte**

Cada objecte representa una petita part de l’aplicació, com si fóssin petites aplicacions per si mateixes. Els objectes son la representació de les clases. 

**Events i propietats**

Les propietats son les característiques actuals d’un objecte, les quals poden ser modificades per diferents events. 

**Encapçulament**

L’encapçulament reconeix que les classes son ideas contingudes en si mateixes, encapsulant les propietats i la data.  L’encapçulament per exemple determina com es pot accedir a un element d’una clase, declarant si es públic, privat o protected. 


