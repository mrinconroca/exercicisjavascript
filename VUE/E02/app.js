const app = Vue.createApp({
    data() {
      return {
        nom: 'Grumpy',
        edat: 4,
        imatge: 'https://marketingaholic.com/wp-content/uploads/2014/12/grumpy-cat-meme-1-1024x576.jpg'
      };
    },

    methods: {
      calcEdat() {
        return this.edat + 5;
      },

      calcRandom() {
        return Math.random();
      }
    }
  });
  
  app.mount('#assignment');