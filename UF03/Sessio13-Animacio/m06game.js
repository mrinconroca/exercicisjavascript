let redLight = document.querySelector("#redLight");
let clickLight = document.querySelector("#clickLight");
let finishlight = document.querySelector("#finishlight");
let fwalk = document.querySelector("#fwalk");
let fwalkrev = document.querySelector("#fwalkrev");
let fsleep = document.querySelector("#fsleep");
let fsleepzero = document.querySelector("#fsleepzero");
let pressto = document.querySelector("#pressTo");
let waddlereverse = document.querySelector("#waddlereverse");
let standbywalk = document.querySelector("#standbywalk");
let waddlespeech = document.querySelector("#waddlespeech");
let accept = document.querySelector("#accept");
let decline = document.querySelector("#decline");
let kirbspeechfite = document.querySelector("#kirbspeechfite");
let kirbspeechpaci = document.querySelector("#kirbspeechpaci");
let waddlewantsfitefite = document.querySelector("#waddlewantsfitefite");
let waddlegotfite = document.querySelector("#waddlegotfite");
let myHP = document.querySelector("#myHP");
let enemyHP = document.querySelector("#enemyHP");
let rock = document.querySelector("#rock");
let blade = document.querySelector("#blade");
let hammer = document.querySelector("#hammer");
let rockpic = document.querySelector("#rockpic");
let bladepic = document.querySelector("#bladepic");
let victoryspeech = document.querySelector("#victoryspeech");
let slashpic = document.querySelector("#slashpic");
let hammerpic = document.querySelector("#hammerpic");
let rockexplain = document.querySelector("#rockexplain");
let slashexplain = document.querySelector("#slashexplain");
let hammerexplain = document.querySelector("#hammerexplain");
let kirbyctory = document.querySelector("#kirbyctory");
let enemydmged = document.querySelector("#enemydmged");
let counterattack = document.querySelector("#counterattack");

var allowed = true;

finishlight.style.display = "none";
fwalk.style.display = "none";
fwalkrev.style.display = "none";
clickLight.style.display = "none";
fsleep.style.display = "none";
waddlereverse.style.display = "none";
standbywalk.style.display = "none";
waddlespeech.style.display = "none";
accept.style.display = "none";
decline.style.display = "none";
kirbspeechfite.style.display = "none";
kirbspeechpaci.style.display = "none";
waddlewantsfitefite.style.display = "none";
waddlegotfite.style.display = "none";
myHP.style.display = "none";
enemyHP.style.display = "none";
rock.style.display = "none";
blade.style.display = "none";
rockpic.style.display = "none";
victoryspeech.style.display = "none";
slashpic.style.display = "none";
hammer.style.display = "none";
hammerpic.style.display = "none";
rockexplain.style.display = "none";
slashexplain.style.display = "none";
hammerexplain.style.display = "none";
kirbyctory.style.display = "none";
enemydmged.style.display = "none";
counterattack.style.display = "none";

rock.addEventListener("mouseover", function(){
    rockexplain.style.display = "initial";
});
rock.addEventListener("mouseout", function(){
    rockexplain.style.display = "none";
});

blade.addEventListener("mouseover", function(){
    slashexplain.style.display = "initial";
});
blade.addEventListener("mouseout", function(){
    slashexplain.style.display = "none";
});

hammer.addEventListener("mouseover", function(){
    hammerexplain.style.display = "initial";
});
hammer.addEventListener("mouseout", function(){
    hammerexplain.style.display = "none";
});

//37 left, 38 up, 39 right, 40 down

document.body.addEventListener("keydown", function (event) {
    if (event.keyCode == 13 && allowed) {
        allowed = false;
        fsleepzero.style.display = "none";
        fsleep.style = "initial";
        pressto.style.display = "none";

        setTimeout(() => {
            redLight.style.display = "none";
            clickLight.style.display = "initial";
        }, 1500)

        setTimeout(() => {
            startAnimation();
        }, 2000)

        setTimeout(() => {
            fwalk.style.display = "none";
            standbywalk.style.display = "initial";
            waddlespeech.style.display = "initial";
            accept.style.display = "initial";
            decline.style.display = "initial";
        }, 6000)
    }

});

let startAnimation = () => {
    clickLight.style.display = "none";
    fwalk.style.display = "initial";
    fsleep.style.display = "none";
    waddlereverse.style.display = "initial";

    let startKeyframes = { "left": "2vw" };
    let endKeyframes = { "left": "30vw" };
    let topKeyFrames = { "top": "2vh" };
    let bottomKeyFrames = { "top": "30vh" };

    let options = {
        duration: 4000
    };

    fwalk.animate([startKeyframes, endKeyframes], options);
    fwalk.animate([topKeyFrames, bottomKeyFrames], options);
};

let finish = () => {
    fwalkrev.style.display = "none";
    finishlight.style.display = "initial";
};

function decision(value) {
    accept.style.display = "none";
    decline.style.display = "none";
    waddlespeech.style.display = "none";
    if (value) {
        kirbspeechfite.style.display = "initial";
        setTimeout(() => {
            kirbspeechfite.style.display = "none";
            waddlegotfite.style.display = "initial";
            setTimeout(() => {
                waddlegotfite.style.display = "none";
            }, 2000)
        }, 2000)
    } else {
        kirbspeechpaci.style.display = "initial";
        setTimeout(() => {
            kirbspeechpaci.style.display = "none";
            waddlewantsfitefite.style.display = "initial";
            setTimeout(() => {
                waddlewantsfitefite.style.display = "none";
            }, 2000)
        }, 2000)
    }
    setTimeout(() => {
        startCombat();
    }, 4000)
}

function startCombat() {
    myHP.style.display = "initial";
    enemyHP.style.display = "initial";
    rock.style.display = "initial";
    blade.style.display = "initial";
    hammer.style.display = "initial";
}

function chechHP() {
    enemydmged.style.display = "none";
    rock.style.display = "none";
    blade.style.display = "none";
    hammer.style.display = "none";

    if (enemyHP.value < 1) {
    myHP.style.display = "none";
    enemyHP.style.display = "none";

    let villianStart = { 
        transform: 'scale(1)'
    };
    let villianEnd = {
        transform: 'scale(0)'
    };
    let options = {"duration": 1500};
    waddlereverse.animate([villianStart, villianEnd], options);

    setTimeout(() => {
        waddlereverse.style.display = "none";
        victoryspeech.style.display = "initial";
        standbywalk.style.display = "none";
        kirbyctory.style.display = "initial";
    }, 1500);

    } else {
        enemyAttack();
    }
}

function move(attack) {
    rock.style.display = "none";
    blade.style.display = "none";
    hammer.style.display = "none";
    enemydmged.style.display = "initial";
    switch (attack) {
        case rock:
            rockpic.style.display = "initial";
            standbywalk.style.display = "none";
            let topKeyFrames = { "top": "8vh" };
            let bottomKeyFrames = { "top": "35vh" };
            let options = {
                duration: 2000,
                easing: "cubic-bezier(0.5, 0, 0.75, 0)"
            };
            rockpic.animate([topKeyFrames, bottomKeyFrames], options);
            setTimeout(() => {
                enemyHP.value = enemyHP.value - 25;
                rockpic.style.display = "none";
                standbywalk.style.display = "initial";
                chechHP();
            }, 2000)
            break;
        case blade:
            slashpic.style.display = "initial";
            standbywalk.style.display = "none";
            setTimeout(() => {
                enemyHP.value = enemyHP.value - 25;
                slashpic.style.display = "none";
                standbywalk.style.display = "initial";
                chechHP();
            }, 2000)
            break;
        case hammer:
            hammerpic.style.display = "initial";
            standbywalk.style.display = "none";
            setTimeout(() => {
                enemyHP.value = enemyHP.value - 25;
                hammerpic.style.display = "none";
                standbywalk.style.display = "initial";
                chechHP();
            }, 2000) 
            break;
    }
}

function enemyAttack(){
    counterattack.style.display = "initial";
    let startKeyframes = { "left": "62vw" };
    let endKeyframes = { "left": "30vw" };

    let options = {
        duration: 1500,
        easing: "cubic-bezier(0.5, 0, 0.75, 0)"
    };

    waddlereverse.animate([startKeyframes, endKeyframes], options);
    setTimeout(() => {
    counterattack.style.display = "none";
    }, 1500) 

    setTimeout(() => {
        myHP.value = myHP.value - 10;
        rock.style.display = "initial";
        blade.style.display = "initial";
        hammer.style.display = "initial";
    }, 1500)
}



