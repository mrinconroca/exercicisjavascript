**Origin:**
JavaScript va ser desenvolupat per a Brendan Eich a l’any 1995, amb el nom oficial sent ECMAScript.

**Versions:**

_ ECMAScript 1  1997 _

Va ser la primera versió de JavaScript. 

Compatible només amb el buscador Netscape 2 i IE 4.

_ ECMAScript 2  1998_

Es van fer canvis editorials. 

Buscador Netscape 4 va ser compatible també.

_ ECMAScript 3  1999_

Es va afegir la compatibilitat amb Expressions Regulars.

Es va afegir l’ús del try/catch.

Compatibilitat amb tots els buscadors

_ ECMAScript 4  ????_

Controversies i poca compatibilitat amb els buscadors principals van provocar que mai fos publicada.

És l’única versió de ECMAScript que mai va ser publicada.

_ ECMAScript 5 (JS ES5)  2009_

Es va afegir “strict mode”.

Es va afegir suport amb JSON.

Es va afegir la possibilitat de fer String.trim().

Es va afegir una manera de comprobar si un Array es un Array amb Array.isArray().

Es van afegir diversas maneres d’iterar arrays.

_ ECMAScript 5.1 - 2011_

Es van fer canvis editorials. 

_ ECMAScript 2015 (JS ES6) - 2015_

Es va afegir let i const.

Es van afegir valors per a parametres predeterminats.

Es va afegir Array.find().

Es va afegir Array.findIndex().

_ ECMAScript 2016 - 2016_

Es va afegir l’operador exponencial (**).

Es va afegir Array.prototype.includes.

_ ECMAScript 2017 - 2017_

Es va afegir padding als string.

Es van afegir noves propietats per als Objects.

Es va afegir l’opció de fer funcions asíncrones.

Es va afegir la memoria compartida

_ ECMAScript 2018  2018_

Es van afegir les propietats de rest/spread.

Es va afegir la iteració d’Asíncrones. 

Es va afegir Promise.finally();

S’ha ampliat el RegEx.



